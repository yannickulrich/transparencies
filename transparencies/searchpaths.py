import pathlib
import os


def root():
    self = pathlib.Path(__file__).parent.resolve()
    devmode = (self.parent/"setup.py").is_file()
    if devmode:
        return self.parent
    else:
        return self.parent.parent.parent.parent/"share"/"transparencies"


def getpath(specs):
    if isinstance(specs, (pathlib.WindowsPath, pathlib.PosixPath)):
        p = specs
    elif type(specs) == str:
        p = pathlib.Path(specs)

    try:
        p = p.resolve()
    except (OSError, RuntimeError):
        return None

    if p.is_dir():
        return p
    else:
        return None


def get_theme(specs):
    if p := getpath(specs):
        return p

    paths = [pathlib.Path(".").resolve()]

    v = os.environ.get("TRANSPARENCIES_THEME_PATH")
    if v:
        paths += [
            pathlib.Path(i).resolve()
            for i in v.split(":")
        ]

    paths.append(root()/"themes")

    for dir in paths:
        if (p := dir/specs).is_dir():
            if (p/"theme").is_dir():
                # this is a submodule
                return p/"theme"
            else:
                return p


def get_plugins():
    paths = []

    v = os.environ.get("TRANSPARENCIES_PLUGIN_PATH")
    if v:
        paths += [
            pathlib.Path(i).resolve()
            for i in v.split(":")
        ]

    paths.append(root()/"plugins")

    for dir in paths:
        if not dir.is_dir():
            continue
        for fn in dir.iterdir():
            if fn.name[0] == ".":
                continue
            if fn.suffix != ".py":
                continue
            yield fn
