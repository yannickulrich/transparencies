import pathlib
import importlib
import mistletoe
import sys
from .searchpaths import get_plugins
import hashlib
import logging

logger = logging.getLogger(__name__)


def error(msg, *args):
    logger.error(msg, *args)
    raise Exception(msg % args)


class plugin:
    def __init__(self, head, opts):
        self.head = head
        self.opts = opts

    def write(self, data, suffix=""):
        fn = hashlib.sha1(data.encode()).hexdigest()
        if suffix:
            fn += suffix
        logger.info("Creating file %s", fn)

        with open(self.head["public"]/fn, "w") as fp:
            fp.write(data)
        return fn


class renderer(plugin):
    def __init__(self, *extra):
        super().__init__(*extra)
        default = mistletoe.HTMLRenderer()

    def render(self, token):
        return default.render_block_code(token)

    def prefix(self):
        return ""

    def suffix(self):
        return ""


class block_renderer(renderer):
    def can_render(self, token):
        return True


class token_renderer(renderer):
    pass


class token:
    renderers = []
    token = None


class global_action(plugin):
    def preprocess_slide(self, slide):
        return slide

    def postprocess_slide(self, html):
        return html


def import_mod(fn, name):
    spec = importlib.util.spec_from_file_location(name, fn)
    foo = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(foo)
    return foo


class plugin_manager:
    def __init__(self, head):
        self.head = head
        self.block_renderers = []
        self.token_renderers = []
        self.global_actions = []
        self.tokens = []

        self._import()

    def _import1(self, fn):
        logger.debug("Loading module %s", fn)
        sys.path.insert(0, fn.parent.resolve().as_posix())
        mod = importlib.import_module(fn.stem)
        sys.path.pop(0)

        if hasattr(mod, "main"):
            return mod.main
        else:
            logger.error("%s is not a valid plugin", mod.__name__)

    def _add(self, c, opts):
        if issubclass(c, block_renderer):
            logger.info("Adding plugin %s as block renderer", c.__name__)
            self.block_renderers.append(c(self.head, opts))
        elif issubclass(c, token_renderer):
            logger.info("Adding plugin %s as token renderer", c.__name__)
            self.token_renderers.append(c(self.head, opts))
        elif issubclass(c, global_action):
            logger.info("Adding plugin %s as global action", c.__name__)
            self.global_actions.append(c(self.head, opts))
        elif issubclass(c, token):
            logger.info(
                "Adding plugin %s as token with %d renderers",
                c.__name__, len(c.renderers)
            )
            c.token.opts = opts
            self.tokens.append(c.token)
            for r in c.renderers:
                self._add(r, opts)

    def _import(self):
        plugins = {fn.stem: fn for fn in get_plugins()}
        if "plugins" not in self.head:
            return
        for p in self.head["plugins"]:
            name = p["name"]
            opts = p.get("args", {})
            if name in plugins:
                c = self._import1(plugins[name])
                self._add(c, opts)
            else:
                error("Can't find plugin %s", name)
