FROM alpine:edge

RUN echo "selected_scheme scheme-infraonly" > prof && \
    echo "TEXDIR /usr/local/texlive" >> prof && \
    echo "TEXMFLOCAL /usr/local/texlive/texmf-local" >> prof && \
    echo "TEXMFSYSCONFIG /usr/local/texlive/texmf-config" >> prof && \
    echo "TEXMFSYSVAR /usr/local/texlive/texmf-var" >> prof && \
    apk add --no-cache wget perl xz && \
    wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz && \
    tar -xzf install-tl-unx.tar.gz && \
    install-tl-20*/install-tl --profile=prof && \
    /usr/local/texlive/bin/x86_64-linuxmusl/tlmgr install \
      latex-bin latex-bin.x86_64-linuxmusl dvisvgm dvips \
      amsfonts amsmath standalone pgf varwidth xkeyval preview && \
    rm -rf /usr/local/texlive/texmf-var/web2c/luatex \
           /usr/local/texlive/texmf-var/web2c/pdftex/{pdflatex.fmt,pdftex.fmt} \
           /usr/local/texlive/texmf-var/web2c/luahbtex \
           /usr/local/texlive/texmf-var/web2c/*.log \
           /usr/local/texlive/texmf-dist/doc \
           /usr/local/texlive/texmf-dist/source \
           /usr/local/texlive/texmf-dist/tex/luatex \
           /usr/local/texlive/bin/x86_64-linuxmsl/kpse* \
           /usr/local/texlive/bin/x86_64-linuxmsl/lua* \
           /usr/local/texlive/tlpkg \
           install-tl-* prof && \
    apk del --no-cache wget xz perl
ENV PATH=/usr/local/texlive/bin/x86_64-linuxmusl:$PATH

RUN apk add --no-cache make gcc musl-dev && \
    wget https://github.com/ArtifexSoftware/ghostpdl-downloads/releases/download/gs9550/ghostscript-9.55.0.tar.gz && \
    tar xf ghostscript-*.tar.gz && \
    cd ghostscript-* && \
    ./configure && make so && cp sobin/libgs.so* /usr/lib && \
    cd / && rm -rf ghostscript* && \
    apk del --no-cache make gcc musl-dev

WORKDIR /opt

COPY . /opt/
RUN apk add --no-cache python3 py3-six py3-pip py3-requests && \
    pip3 install --no-cache-dir -r requirements.txt && \
    pip3 install --no-cache-dir . && \
    apk del --no-cache py3-pip && \
    rm -rf *
