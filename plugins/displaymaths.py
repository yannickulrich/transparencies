import re
import transparencies.plugins
from tex import tex_renderer


class texblock_renderer(transparencies.plugins.block_renderer, tex_renderer):
    default_preamble = "\\usepackage{amsmath}\\usepackage{amssymb}"
    default_template = (
        r"\documentclass[preview]{standalone}"
        r"{{preamble}}"
        r"\begin{document}"
        r"$\displaystyle\begin{aligned}"
        r"{{body}}"
        r"\end{aligned}$"
        r"\end{document}"
    )

    def can_render(self, token):
        return token.language == 'math'

    def render_block_code(self, token):
        code = token.children[0].content
        return self.typeset(code, classes=["maths", "display"])


main = texblock_renderer
